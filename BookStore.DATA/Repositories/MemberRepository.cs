﻿//<auto-generated/>
// <copyright file="MemberRepository.cs" company="K1BBQ_B7QEII_GT684Q">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookStore.DATA.Repositories
{
    using BookStore.DATA.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    /// <summary>
    /// Tag repó
    /// </summary>
    public class MemberRepository : IMemberRepository
    {
        private BookStoreDBEntities entities;
        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="entities">Adatbázis referencia</param>
        public MemberRepository(BookStoreDBEntities entities)
        {
            this.entities = entities;
        }
        /// <summary>
        /// Törli a beadott tagot az adatbázisból.
        /// </summary>
        /// <param name="member">Tag referencia</param>
        public void Delete(Member member)
        {
            this.entities.Members.Remove(member);
            this.entities.SaveChanges();
        }
        /// <summary>
        /// Vissszadja a tagok listáját.
        /// </summary>
        /// <returns>Tagok lista</returns>
        public IQueryable<Member> GetAll()
        {
            return entities.Members;
        }
        /// <summary>
        /// Elmenti a beadott tagot az adatbázisba.
        /// </summary>
        /// <param name="member">Tag referencia</param>
        public void Save(Member member)
        {


            bool newMember = !entities.Members.Select(m => m.memberID).Contains(member.memberID);
            try
            {
                if (newMember)
                {
                    entities.Members.Add(member);
                }
                entities.SaveChanges();

            }
            catch (Exception)
            {

                if (newMember)
                {
                    entities.Members.Remove(member);
                }

                entities.Entry(member).Reload();

                throw;
            }
        }
    }
}
