var searchData=
[
  ['bl',['BL',['../namespace_book_store_1_1_b_l.html',1,'BookStore']]],
  ['bl_5ftests',['BL_Tests',['../namespace_book_store_1_1_b_l___tests.html',1,'BookStore']]],
  ['bookstore',['BookStore',['../namespace_book_store.html',1,'']]],
  ['data',['DATA',['../namespace_book_store_1_1_d_a_t_a.html',1,'BookStore']]],
  ['gui',['GUI',['../namespace_book_store_1_1_g_u_i.html',1,'BookStore']]],
  ['gui_5ftests',['GUI_Tests',['../namespace_book_store_1_1_g_u_i___tests.html',1,'BookStore']]],
  ['interfaces',['Interfaces',['../namespace_book_store_1_1_b_l_1_1_interfaces.html',1,'BookStore.BL.Interfaces'],['../namespace_book_store_1_1_d_a_t_a_1_1_interfaces.html',1,'BookStore.DATA.Interfaces']]],
  ['properties',['Properties',['../namespace_book_store_1_1_g_u_i_1_1_properties.html',1,'BookStore::GUI']]],
  ['repositories',['Repositories',['../namespace_book_store_1_1_d_a_t_a_1_1_repositories.html',1,'BookStore::DATA']]],
  ['tests',['Tests',['../namespace_book_store_1_1_b_l_1_1_tests.html',1,'BookStore.BL.Tests'],['../namespace_book_store_1_1_g_u_i_1_1_tests.html',1,'BookStore.GUI.Tests']]]
];
