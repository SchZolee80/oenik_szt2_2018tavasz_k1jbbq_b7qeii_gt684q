var indexSectionsWithContent =
{
  0: "_abcdefghijkmnopqrstuvwxy~",
  1: "abcdefijkmopqrstuvw",
  2: "bdknop",
  3: "adfgmrst",
  4: "_acdefgijkmnopqrstuvw~",
  5: "bcdeghimopsvxy",
  6: "cotuv",
  7: "aegt",
  8: "egtv",
  9: "s",
  10: "abm",
  11: "t",
  12: "brt"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "related",
  10: "defines",
  11: "groups",
  12: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Friends",
  10: "Macros",
  11: "Modules",
  12: "Pages"
};

