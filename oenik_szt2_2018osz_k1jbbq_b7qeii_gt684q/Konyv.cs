﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oenik_szt2_2018osz_k1jbbq_b7qeii_gt684q
{
    class Konyv : IKonyv
    {
        private int konyvszamlalo = 1000; 

        private int konyvID;

        public int KonyvID
        {
            get { return konyvID; }
            set { konyvID = value; }
        }


        private string cim;

        public string Cim
        {
            get { return cim; }
            set { cim = value; }
        }

        private List<string> iro;

        public List<string> Iro
        {
            get { return iro; }
            set { iro = value; }
        }

        private string kiado;

        public string Kiado
        {
            get { return kiado; }
            set { kiado = value; }
        }


        private int kiadaseve;

        public int Kiadaseve
        {
            get { return kiadaseve; }
            set { kiadaseve = value; }
        }

        private List<string> mufaj;

        public List<string> Mufaj
        {
            get { return mufaj; }
            set { mufaj = value; }
        }

        private bool kolcsonzozve;

        public bool Kolcsonozve
        {
            get { return kolcsonzozve; }
            set { kolcsonzozve = value; }
        }


        public Konyv(string cim, List<string> iro, string kiado, int kiadaseve, List<string> mufaj)
        {
            konyvszamlalo++;
            this.KonyvID = konyvszamlalo;
            this.Cim = cim;
            this.Iro = iro;
            this.Kiado = kiado;
            this.Kiadaseve = kiadaseve;
            this.Mufaj = mufaj;
            this.Kolcsonozve = false;
        }

        public void KonyvLetrehoz(string cim, List<string> irok, string kiado, int kiadasEve, List<string> mufaj)
        {
            throw new NotImplementedException();
        }

        public Konyv Keres(string cim)
        {
            throw new NotImplementedException();
        }

        public Konyv Keres(int konyvID)
        {
            throw new NotImplementedException();
        }

        public bool KonyvTorol(int konyvID)
        {
            throw new NotImplementedException();
        }

        public bool KivanEKolcsonozve(int konvyID)
        {
            throw new NotImplementedException();
        }
    }
}