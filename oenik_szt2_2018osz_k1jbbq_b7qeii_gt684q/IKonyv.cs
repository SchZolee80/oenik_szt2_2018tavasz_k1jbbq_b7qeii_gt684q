﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oenik_szt2_2018osz_k1jbbq_b7qeii_gt684q
{
    interface IKonyv
    {
        /// <summary>
        /// Könyv létrehozása.
        /// </summary>
        /// <param name="cim">Cím</param>
        /// <param name="irok">írók</param>
        /// <param name="kiado">Kiadó</param>
        /// <param name="kiadasEve">Kiadási év</param>
        /// <param name="mufaj">Műfajok</param>
        void KonyvLetrehoz(string cim, List<string> irok, string kiado, int kiadasEve, List<string> mufaj);
        /// <summary>
        /// Keresés cím alapján.
        /// </summary>
        /// <param name="cim">Keresendő cím.</param>
        /// <returns></returns>
        Konyv Keres(string cim);
        /// <summary>
        /// Keresés könyv ID alapján.
        /// </summary>
        /// <param name="konyvID">Könyv ID</param>
        /// <returns></returns>
        Konyv Keres(int konyvID);
        /// <summary>
        /// Törli a könyvet és visszaadja, hogy sikeres volt e.
        /// </summary>
        /// <param name="konyvID"></param>
        /// <returns></returns>
        bool KonyvTorol(int konyvID);
        /// <summary>
        /// Ki van e kölcsönözve.
        /// </summary>
        /// <param name="konvyID">Könyv ID</param>
        /// <returns></returns>
        bool KivanEKolcsonozve(int konvyID);

    }
}
