﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oenik_szt2_2018osz_k1jbbq_b7qeii_gt684q
{
    class Kolcsonzes : IKolcsonzes
    {
        private int kolcsonzesszamlalo = 1000;

        private int kolcsonzesID;

        public int KolcsonzesID
        {
            get { return kolcsonzesID; }
            set { kolcsonzesID = value; }
        }

        private int tagID;

        public int TagID
        {
            get { return tagID; }
            set { tagID = value; }
        }

        private int konyvID;

        public int KonyvID
        {
            get { return konyvID; }
            set { konyvID = value; }
        }

        private DateTime kezdet;

        public DateTime Kezdet
        {
            get { return kezdet; }
            set { kezdet = value; }
        }

        private DateTime vege;

        public DateTime Vege
        {
            get { return vege; }
            set { vege = value; }
        }

        private double dij;

        public double Dij
        {
            get { return dij; }
            set { dij = value; }
        }

        public Kolcsonzes(int tagID, int konyvID, DateTime kezdet, DateTime vege, double dij)
        {
            kolcsonzesszamlalo++;
            this.KolcsonzesID = kolcsonzesszamlalo;
            this.TagID = tagID;
            this.KonyvID = konyvID;
            this.Kezdet = kezdet;
            this.Vege = vege;
            this.Dij = dij;
        }

        public void KolcsonzesFelvetel(int tagID, int konyvID, DateTime kezdet, DateTime vege, double dij)
        {
            throw new NotImplementedException();
        }

        public bool KolcsonzesTorles(int kolcsonzesID)
        {
            throw new NotImplementedException();
        }

        public bool KolcsonzesModosit(int kolcsonzesID)
        {
            throw new NotImplementedException();
        }

        public Kolcsonzes Keres(bool? melyik, int kolcsonzesID)
        {
            throw new NotImplementedException();
        }
    }
}
