using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oenik_szt2_2018osz_k1jbbq_b7qeii_gt684q
{
    class Tag : ITag
    {
        private int tagIDszamlalo = 1000;

        private int tagID;

        public int TagID
        {
            get { return tagID; }
            set { tagID = value; }
        }

        private string nev;

        public string Nev
        {
            get { return nev; }
            set { nev = value; }
        }

        private DateTime szuldatum;

        public DateTime Szuldatum
        {
            get { return szuldatum; }
            set { szuldatum = value; }
        }


        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        //private string jelszo;

        //public string Jelszo
        //{
        //    get { return jelszo; }
        //    set { jelszo = value; }
        //}

        public Tag(string nev, DateTime szuldatum, string email)
        {
            tagIDszamlalo++;
            this.TagID = tagIDszamlalo;
            this.Szuldatum = szuldatum;
            this.Email = email;
        }

        public void TagFelvesz(string nev, string email, DateTime szuldatum)
        {
            throw new NotImplementedException();
        }

        public Tag TagKeres(string nev, string email)
        {
            throw new NotImplementedException();
        }

        public bool TagTorol(string nev, string email)
        {
            throw new NotImplementedException();
        }

        public bool TagModosit(string nev, string email, DateTime szuldatum)
        {
            throw new NotImplementedException();
        }
    }
}