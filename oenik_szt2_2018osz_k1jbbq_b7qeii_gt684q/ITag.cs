﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oenik_szt2_2018osz_k1jbbq_b7qeii_gt684q
{
    interface ITag
    {
        /// <summary>
        /// Tag felvétele.
        /// </summary>
        /// <param name="nev">Név</param>
        /// <param name="email">Email cím.</param>
        /// <param name="szuldatum">Szöletési dátum</param>
        void TagFelvesz(string nev, string email, DateTime szuldatum);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nev"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        Tag TagKeres(string nev, string email);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nev"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        bool TagTorol(string nev, string email);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nev"></param>
        /// <param name="email"></param>
        /// <param name="szuldatum"></param>
        /// <returns></returns>
        bool TagModosit(string nev, string email, DateTime szuldatum);
    }
}
