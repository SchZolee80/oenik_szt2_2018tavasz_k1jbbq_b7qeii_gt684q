﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oenik_szt2_2018osz_k1jbbq_b7qeii_gt684q
{
    interface IKolcsonzes
    {
        void KolcsonzesFelvetel(int tagID, int konyvID, DateTime kezdet, DateTime vege, double dij);
        bool KolcsonzesTorles(int kolcsonzesID);
        bool KolcsonzesModosit(int kolcsonzesID);
        /// <summary>
        /// Keresés kolcsonzesID=null, konyv=true, tag=false
        /// </summary>
        /// <param name="melyik">Mi alapján.</param>
        /// <param name="kolcsonzesID"></param>
        /// <returns></returns>
        Kolcsonzes Keres(bool? melyik, int kolcsonzesID);
        
    }
}
